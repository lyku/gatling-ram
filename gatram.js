var fs = require('fs');
var http = require('http');
var https = require('https');

var CONFIG, SERV, PORT, CONFIGPATH = "gatram-config.json", WEBROOT = "webroot";
var FILES = {};
var FILENAMES = [];
var DIRECTORIES = [];
var INDEXES = {};
var INDEXNAMES = [];

//Allow reading from URL directories...?

var QUEUED_LOADS = 0;

//log(process.argv[2])
if(process.argv.length==3) {
	CONFIGPATH = process.argv[2];
	log("Loading temporal config.");
}

fs.exists('gatram-config.json', exists => {
	if(exists) readConfig()
	else fs.writeFile(CONFIGPATH, '{}', (err) => {
		if(err) {
			log("Error creating config file.");
		} else {
			readConfig();
		}
	})
})

function readConfig() {
	fs.readFile(CONFIGPATH, (err, body) => {
		if(err) {
			//log(err);
			log("Error reading config file.");
		} else {
			log(body.toString())
			try {
				CONFIG = JSON.parse(body.toString());
			} catch(except) {
				log("Error parsing configs." + except)
			}
			if(CONFIG) config();
		}
	})
}

function config() {
	var options = {};
	
	if(CONFIG.webroot) {
		WEBROOT = CONFIG.webroot;
		//if(!WEBROOT.match(/\/$/))WEBROOT+="/";
	}
	
	loadFiles();
}

function listener(req, res) {
	var path = req.url.split(/\?|#/)[0];
	//log(path)
	var data;
	if(FILENAMES.indexOf(path)>=0) {
		res.end(FILES[path]);
	} else if(INDEXNAMES.indexOf(path)>=0) {
		path = INDEXES[path];
		res.end(FILES[path]);
	} else {
		res.end(FILENAMES.indexOf('404.html')>=0?FILES['404.html']:'<h1>404: File not found.</h1>');
	}
	
	
	//res.end(req.url)
	//res.end('AYYYYYYYYY!');
}

function loadFiles() {
	if(fs.existsSync(WEBROOT)) {
		loadDirectory(WEBROOT);
	} else {
		log("Webroot does not exist: " + WEBROOT);
	}
}

function loadDirectory(directory) {
	QUEUED_LOADS ++;
	DIRECTORIES.push(directory.substring(WEBROOT.length));
	var contents = fs.readdirSync(directory);
	for(var i = 0; i < contents.length; i ++) {
		var f = directory+"/"+contents[i];
		var stat = fs.statSync(f);
		(stat.isDirectory()?loadDirectory:loadFile)(f)
	}
	tryDoneLoading();
}

function loadFile(file) {
	QUEUED_LOADS ++;
	fs.readFile(file, (err, res) => {
		if(err) {
			log("Error reading " + file);
		} else {
			FILES[file.substring(WEBROOT.length)] = res;
			log("loaded " + file)
			tryDoneLoading();
		}
	})
}
function tryDoneLoading() {
	//log(QUEUED_LOADS);
	if(!--QUEUED_LOADS)launchHTTP();
}

function launchHTTP() {
	
	FILENAMES = Object.keys(FILES);
	for(var i = 0; i < FILENAMES.length; i ++) {
		var f = FILENAMES[0];
		var reg = /([^\/]*\/)+(.*\.(html|gtr))/;
		var m = f.match(reg);
		if(m && m.length > 1) {
			INDEXES[m[1]] = f;
		}
	}
	INDEXNAMES = Object.keys(INDEXES);
	
	//log(FILES)
	
	if(CONFIG.port) {
		if(typeof CONFIG.port != 'number') {
			log("Invalid port of " + CONFIG.port);
			return;
		}
		PORT = CONFIG.port;
	}
	if(CONFIG.https) {
		//var options = {};
		var paths = CONFIG.https;
		if(paths.key)  {
			options.key = fs.readFileSync(paths.key);
			if(!options.key) {
				log("Error reading key " + paths.key);
				return;
			}
		}
		if(paths.cert) {
			options.cert = fs.readFileSync(paths.cert);
			if(!options.cert) {
				log("Error reading cert " + paths.cert);
				return;
			}
		}
		if(paths.fullchain) {
			options.fullchain = fs.readFileSync(paths.fullchain);
			if(!options.fullchain) {
				log("Error reading fullchain " + paths.fullchain);
				return;
			}
		}
		SERV = https.createServer(options, listener).listen(PORT || 443);
	} else {
		SERV = http.createServer(listener).listen(PORT || 80);
	}
}

function rend(res, json){if(res&&res.end)res.end(JSON.stringify(json));else log("Cannot end res.")}
function log(text){console.log(text)}